export default class ObjectPropertyFilter {
    private whitelistedProperties = new Map<string, { isArray: boolean, filter: ObjectPropertyFilter } | undefined>();

    public constructor(template?: string) {
        if (template) {
            this.addTemplate(...template.split(','));
        }
    }

    public addTemplate(...template: string[]) {
        for (let subtemplate of template) {
            this.add(...subtemplate.split('.'));
        }
    }

    public add(...filter: string[]) {
        if (filter.length < 1) return;

        let first = filter.shift() as string; // is always string, never undefined, we just checked array length
        let isArray: boolean;
        if (first.endsWith('[]')) {
            isArray = true;
            first = first.substr(0, first.length - 2);
        } else {
            isArray = false;
        }

        if (this.whitelistedProperties.has(first)) {
            // whitelist entry already exists, get it
            let subfilter = this.whitelistedProperties.get(first);
            if (subfilter === undefined) {
                // no existing subfilter
                if (filter.length > 0) {
                    // we have a subfilter now, lets assing it
                    let subfilter = new ObjectPropertyFilter();
                    subfilter.add(...filter);
                    this.whitelistedProperties.set(first, { filter: subfilter, isArray: isArray });
                } else {
                    // no existing subfilter, no new subfilter, everythig is good, no need to do anything
                }
            } else {
                // we have an existing subfilter, lets check if the isArray value is the same
                if (subfilter.isArray != isArray) {
                    // we cant have properties both being an array and not being an array, error out
                    throw new Error(`Property ${first} can't be simultaneously both an array and not an array`);
                }

                if (filter.length > 0) {
                    // existing subfilter, and new subfilter entries, lets add them
                    subfilter.filter.add(...filter);
                } else {
                    // existing subfilter, no new subfilter entries, everything is good, no need to do anything
                }
            }
        } else {
            // whitelist entry doesnt exist yet, need to create it
            if (filter.length > 0) {
                // has subfilter
                let subfilter = new ObjectPropertyFilter();
                subfilter.add(...filter);
                this.whitelistedProperties.set(first, { filter: subfilter, isArray: isArray });
            } else {
                // doesnt have subfilter
                this.whitelistedProperties.set(first, undefined);
            }
        }
    }

    public filterObject(obj: any): any {
        if (obj === null || (typeof obj) !== 'object') return obj;
        let ret: any = {};
        for (let [key, x] of this.whitelistedProperties) {
            let val = obj[key];
            let newVal: any;
            if (x) {
                let { isArray, filter } = x;
                let valIsArray = Array.isArray(val);
                if (isArray != valIsArray) {
                    throw new Error(`Property ${key} either was not an array while it is required to be, or vice versa`);
                }
                if (valIsArray) {
                    newVal = val.map((x: any) => filter.filterObject(x));
                } else {
                    newVal = filter.filterObject(val);
                }
            } else {
                newVal = val;
            }

            if (newVal !== undefined) ret[key] = newVal;
        }
        return ret;
    }

    public toDebugString(level = 0) {
        let str = '';
        for (let i = 0; i < level; ++i) {
            str += '    ';
        }
        str += '{\n';
        for (let x of this.whitelistedProperties) {
            for (let i = 0; i < level; ++i) {
                str += '    ';
            }
            str += ' - ' + x[0];
            if (x[1]) {
                if (x[1].isArray) {
                    str += '[]';
                }
                str += ':\n';
                str += x[1].filter.toDebugString(level + 1);
            }
            str += '\n';
        }
        for (let i = 0; i < level; ++i) {
            str += '    ';
        }
        str += '}';
        return str;
    }
}


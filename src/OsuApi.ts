import config from 'config';
import https from 'https';
import querystring from 'querystring';
import * as RedisCache from './RedisCache';
import { Readable } from 'stream';
import zlib from 'zlib';
import { RateLimiter } from 'limiter';
import http from 'http';
import fs from 'fs';
import path from 'path';
import mkdirp from 'mkdirp';

interface Params {
    [key: string]: string | number | boolean;
}

const OSU_API_KEY = config.get<string>('osu-api.key');
const OSU_API_BAD_RESULTS_LOG_DIR = path.resolve(config.get<string>('osu-api.bad-api-results-log-dir'));
const OSU_API_HTTPS_REQUEST_AGENT = new https.Agent({ keepAlive: true, maxSockets: 1 });
const OSU_API_REQUEST_OPTIONS: https.RequestOptions = {
    timeout: config.get('osu-api.timeout'),
    agent: OSU_API_HTTPS_REQUEST_AGENT,
    headers: {
        'Accept-Encoding': 'br, gzip, deflate'
    }
};
const OSU_API_LIMITER = new RateLimiter(config.get('osu-api.reqsPerMinute'), 'minute');
const CACHE_TIMES: { [key: string]: number } = config.get('cacheTimes');

function buildCacheKey(endpoint: string, params: Params): string {
    return `api:${endpoint}:${RedisCache.createCacheKeyFromObject(params, true)}`;
}

//#region general api things

function mkdirpAsync(dir: string) : Promise<mkdirp.Made> {
    return new Promise((resolve, reject) => {
        mkdirp(dir, (err, result) => {
            if(err) {
                reject(err);
            } else {
                resolve(result);
            }
        })
    });
}

async function logApiErrorResponse(url:string, res: http.IncomingMessage, filename: string, errorMessage:string) {
    await mkdirpAsync(OSU_API_BAD_RESULTS_LOG_DIR);
    let filepath = path.resolve(OSU_API_BAD_RESULTS_LOG_DIR, filename);
    let p = new Promise((resolve, reject) => {
        let f = fs.createWriteStream(filepath);
        f.once('error', reject);
        f.write(`url: ${url}\r\nerror message: ${errorMessage}\r\n---------------\r\nHTTP/${res.httpVersion} ${res.statusCode} ${res.statusMessage}\r\n`);
        for(let i = 0; i<res.rawHeaders.length; i+=2) {
            let key = res.rawHeaders[i];
            let val = res.rawHeaders[i+1];
            f.write(`${key}: ${val}\r\n`);
        }
        f.write('\r\n');
        res.pipe(f);
        res.once('end', resolve);
    });
    await p;
}

function handleRemoteApiError(requestUrl:string, res: http.IncomingMessage, errorMessage: string, doneHandler: (err:any) => void) {
    const errorCode = `${res.statusCode}_${Date.now()}`;
    logApiErrorResponse(requestUrl, res, errorCode + '.txt', errorMessage).then(() => {
        let err: any = new Error(`[${errorCode}] ${errorMessage}`);
        err.osuServerError = true;
        doneHandler(err);
    }).catch(doneHandler);
}

function doActualOsuApiRequestUnsafe(endpoint: string, params: Params): Promise<any> {
    let url = `https://osu.ppy.sh/api/${endpoint}?k=${OSU_API_KEY}&${querystring.stringify(params)}`;

    return new Promise((resolve, reject) => {
        OSU_API_LIMITER.removeTokens(1, () => {
            https.get(url, OSU_API_REQUEST_OPTIONS, res => {
                if (res.statusCode != 200) {
                    return handleRemoteApiError(url, res, `Unexpected osu api response ${res.statusCode} ${res.statusMessage}`, reject);
                }

                let readable: Readable;
                let contentEncoding = res.headers["content-encoding"];
                switch (contentEncoding) {
                    case undefined:
                        // no content encoding
                        readable = res;
                        break;
                    case 'gzip':
                        // gzip compression
                        readable = res.pipe(zlib.createGunzip());
                        break;
                    case 'br':
                        // brotli compression
                        readable = res.pipe(zlib.createBrotliDecompress());
                        break;
                    case 'deflate':
                        // deflate compression
                        readable = res.pipe(zlib.createInflate());
                        break;
                    default:
                        // unknown
                        return handleRemoteApiError(url, res, `unknown content encoding (${contentEncoding}) in api response`, reject);
                }

                // read data
                let chunks: Buffer[] = [];
                let totalDataLength = 0;
                readable.on('data', chunk => {
                    let b = chunk as Buffer;
                    totalDataLength += b.length;
                    chunks.push(b);
                });
                readable.once('end', () => {
                    let data = Buffer.concat(chunks, totalDataLength).toString('utf8');

                    // parse json
                    let j;
                    try {
                        j = JSON.parse(data);
                    }
                    catch (e) {
                        return reject(e);
                    }

                    if (j.error) {
                        // error from api
                        return reject(new Error(j.error));
                    }

                    resolve(j); // resolve json
                });
            });
        });
    });
}

let pendingApiCalls: Promise<any>[] = [];
async function doActualOsuApiRequest(endpoint: string, params: Params): Promise<any> {
    // wait till there is no pending api calls anymore;
    while (pendingApiCalls.length > 0) await Promise.all(pendingApiCalls);
    // then start it and add it to list
    let p = doActualOsuApiRequestUnsafe(endpoint, params);
    pendingApiCalls.push(p);

    // wait for it to finish
    let result = await p;

    // after its done, remove it from the list
    pendingApiCalls.splice(pendingApiCalls.indexOf(p), 1);

    // and finally return the actual result
    return result;
}

async function doApiRequest(endpoint: string, params: Params, customCacheAction?: (body: any, params: Params, cb: (forgedParams: Params, forgedValue: any) => void) => void) {
    let cacheKey = buildCacheKey(endpoint, params);
    let { inCache, result } = await RedisCache.get(cacheKey);
    if (inCache) return result;

    // cache didnt exist, lets get it
    let body = await doActualOsuApiRequest(endpoint, params);

    // if customCacheAction, caller is responsible for storing the value in cache
    let cacheTime = CACHE_TIMES[endpoint] || 0;
    if (customCacheAction) {
        customCacheAction(body, params, (forgedParams, forgedValue) => {
            let forgedKey = buildCacheKey(endpoint, forgedParams);
            RedisCache.storeInCache(cacheTime, forgedKey, forgedValue);
        });
    } else {
        RedisCache.storeInCache(cacheTime, cacheKey, body);
    }

    return body;
}

async function doApiRequestAndGetFirst(endpoint: string,
    params: Params,
    customCacheAction?: (body: any, params: Params, cb: (forgedParams: Params, forgedValue: any) => void) => void) {
    let list = await doApiRequest(endpoint, params, customCacheAction);
    return list[0];
}

//#endregion

//#region beatmap things

function saveCustomCacheForBeatmapObject(b: any, m: number | undefined, a: number, mods: number, saveCallback: (forgedParams: Params, forgedValue: any) => void) {
    // if mode not suppied, it'll give result regardless of the mode of the map (aka everything is "allowed"), but in that case we know that if we called the thing with that mode it would have given the same result
    // so in that case: store all the things with, and without the mode
    // if mode was supplied, store it with that mode. And if the supplied mode matches the mode of the beatmap, also store it without mode
    let forgedValue = [b];
    if (m !== undefined) {
        saveCallback({ b: b.beatmap_id, m: m, a: a, mods: mods }, forgedValue);
        saveCallback({ h: b.file_md5, m: m, a: a, mods: mods }, forgedValue);
        if (m == b.mode) {
            saveCallback({ b: b.beatmap_id, a: a, mods: mods }, forgedValue);
            saveCallback({ h: b.file_md5, a: a, mods: mods }, forgedValue);
        }
    } else {
        saveCallback({ b: b.beatmap_id, m: b.mode, a: a, mods: mods }, forgedValue);
        saveCallback({ h: b.file_md5, m: b.mode, a: a, mods: mods }, forgedValue);
        saveCallback({ b: b.beatmap_id, a: a, mods: mods }, forgedValue);
        saveCallback({ h: b.file_md5, a: a, mods: mods }, forgedValue);
    }
}

function customCacheActionForGetBeatmap(value: any[], originalParams: Params, saveCallback: (forgedParams: Params, forgedValue: any) => void) {
    if (value.length === 0) {
        // empty response, just store, nothing special to do
        return saveCallback(originalParams, value);
    }

    let a = originalParams.a as number;
    let mods = originalParams.mods as number;
    let m;
    if (originalParams.hasOwnProperty('m')) {
        m = originalParams.m as number;
    } else {
        m = undefined;
    }
    saveCustomCacheForBeatmapObject(value[0], m, a, mods, saveCallback);
}

export function getBeatmapById(id: number, mode?: number, includeConverts: boolean = false, mods = 0) {
    let options: Params = { b: id, a: (includeConverts ? 1 : 0), mods: mods };
    if (mode !== undefined) {
        options.m = mode;
    }
    return doApiRequestAndGetFirst('get_beatmaps', options, customCacheActionForGetBeatmap);
}

export function getBeatmapByHash(hash: string, mode?: number, includeConverts: boolean = false, mods = 0) {
    let options: Params = { h: hash, a: (includeConverts ? 1 : 0), mods: mods };
    if (mode !== undefined) {
        options.m = mode;
    }
    return doApiRequestAndGetFirst('get_beatmaps', options, customCacheActionForGetBeatmap);
}

export function getBeatmapSetById(setId: number, mode?: number, includeConverts: boolean = false, mods = 0) {
    let a = includeConverts ? 1 : 0;
    let options: Params = { s: setId, a: a, mods: mods };
    if (mode !== undefined) {
        options.m = mode;
    }
    return doApiRequest('get_beatmaps', options, (value, originalParams, saveCallback) => {
        // we are now responsible for storing the valuy in cache, save the whole s response
        saveCallback(originalParams, value);

        // create forged cache entries for each diff as if a per-diff-api call was done
        for (let b of value) {
            saveCustomCacheForBeatmapObject(b, mode, a, mods, saveCallback);
        }
    });
}

//#endregion

//#region user things

export function getUserById(id: number, mode?: number) {
    return getUser(id, 'id', mode);
}
export function getUserByName(username: string, mode?: number) {
    return getUser(username, 'string', mode);
}
export function getUser(u: number | string, type?: string, mode: number = 0) {
    let options: Params = { u: u, m: mode };
    if (type !== undefined) {
        options.type = type;
    }
    return doApiRequestAndGetFirst('get_user', options, (value, originalParams, saveCallback) => {
        if (value.length === 0) {
            // empty response, just store, nothing special to do
            return saveCallback(originalParams, value);
        }

        let u = value[0];
        // store with supplied mode for username, id and unspecified type
        saveCallback({ u: u.user_id, type: 'id', m: originalParams.m }, value); // with mode, use username
        saveCallback({ u: u.username, type: 'string', m: originalParams.m }, value); // with mode, use username
        // dont save unspecified, if type was specified, since the logic for picking is *technically* undefined
        if (!originalParams.hasOwnProperty('type')) {
            // type was not specified in original request, so gotta save that request as well
            saveCallback({ u: originalParams.u, m: originalParams.m }, value); // with mode, use not specified
        }
    });
}



//#endregion

//#region mp

export function getMpById(mp: number) {
    return doApiRequest('get_match', { mp: mp });
}

//#endregion
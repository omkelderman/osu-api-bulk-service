import { Controller, Post, Route, Body, SuccessResponse } from 'tsoa';
import { getUsersBulk } from '../bulk-operations/users';
import { getMpBulk } from '../bulk-operations/mp';
import { getBeatmapsBulk } from '../bulk-operations/beatmaps';
import { UserBulkRequestParams, MpBulkRequestParams, BeatmapBulkRequestParams } from '../bulk-operations/_shared';
import http from 'http';
import https from 'https';

const MS_1_MINUTE = 1000 * 60;

interface UserBulkRequest {
    action: 'users' | 'users',
    params: UserBulkRequestParams
}
interface MpBulkRequest {
    action: 'mp' | 'mp',
    params: MpBulkRequestParams
}
interface BeatmapBulkRequest {
    action: 'beatmaps' | 'beatmaps',
    params: BeatmapBulkRequestParams
}
interface BulkRequestParams {
    [key: string]: UserBulkRequest | MpBulkRequest | BeatmapBulkRequest
}
interface BulkRequest {
    action: 'bulk' | 'bulk',
    params: BulkRequestParams
}

interface AsyncReqBody {
    url: string,
    request: BulkRequest | UserBulkRequest | MpBulkRequest | BeatmapBulkRequest
}

@Route('async')
@SuccessResponse('200', 'successful')
export class AsyncController extends Controller {
    @Post()
    public async doAsyncReq(@Body() req: AsyncReqBody): Promise<{ status: number, error?: string }> {
        let url: URL;
        try {
            url = new URL(req.url);
            if (url.protocol !== 'https:' && url.protocol !== 'http:') {
                // invalid protocol
                this.setStatus(400);
                return { status: 400, error: 'Invalid URL' };
            }
        } catch (e) {
            this.setStatus(400);
            return { status: 400, error: 'Invalid URL' };
        }

        this.scheduleAsyncRequest(url, req.request);

        this.setStatus(200);
        return { status: 200 };
    }

    private scheduleAsyncRequest(url: URL, request: BulkRequest | UserBulkRequest | MpBulkRequest | BeatmapBulkRequest) {
        var p = this.handleAsyncBulkRequest(request);
        p.then(result => {
            this.runWebHook(url, { success: true, result: result });
        }).catch(err => {
            this.runWebHook(url, { success: false, error: err });
        });
    }
    private runWebHook(url: URL, content: any) {
        const jsonString = JSON.stringify(content);
        const buf = Buffer.from(jsonString, 'utf8');

        const requestOptions : http.RequestOptions = {
            method: 'POST',
            timeout: MS_1_MINUTE,
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Content-Length': buf.length
            }
        };
        const req = this.doHttpOrHttpsRequest(url, requestOptions, res => {
            if(res.statusCode && res.statusCode < 400) {
                // we gud
                console.log(`Webhook (${url}) success!`);
                return;
            }

            // we not gud
            console.error(`ERROR doing wehbook (${url}), invalid response, http version: ${res.httpVersion}, http statuscode: ${res.statusCode}, headers:`, res.headers);
        });

        req.on('error', e => console.error(`ERROR while sending webhook (${url}): `, e));
        req.write(buf);
        req.end();
    }
    
    private doHttpOrHttpsRequest(url: URL, options: http.RequestOptions, callback: (res: http.IncomingMessage) => void) : http.ClientRequest {
        switch(url.protocol) {
            case 'http:': return http.request(url, options, callback);
            case 'https:': return https.request(url, options, callback);
            default: throw new Error('Unknown protocol');
        }
    }

    private async handleAsyncBulkRequest(req: BulkRequest | UserBulkRequest | MpBulkRequest | BeatmapBulkRequest) {
        switch (req.action) {
            case 'users':
                return getUsersBulk(req.params);
            case 'mp':
                return getMpBulk(req.params);
            case 'beatmaps':
                return getBeatmapsBulk(req.params);
            case 'bulk':
                return this.handleBulkAsyncReq(req.params);
            default:
                return Promise.resolve(undefined);
        }
    }

    private async handleBulkAsyncReq(params: BulkRequestParams) {
        let returnObj: { [key: string]: any } = {};
        for (let key in params) {
            returnObj[key] = await this.handleAsyncBulkRequest(params[key]);
        }
        return returnObj;
    }
}
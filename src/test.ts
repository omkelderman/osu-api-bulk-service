import ObjectPropertyFilter from "./ObjectPropertyFilter";

let kaas = new ObjectPropertyFilter('a,b.b1[].q,c,a.f,b.b1[].g[].3');

let ham = kaas.filterObject({
    a:{
        f:[1,2,3,4],
        g: 456
    },
    b: {
        b1: [
            {
                q: 6,
                r: 5,
                g: [
                    {
                        '3': 3,
                        '4': 4
                    },
                    {
                        '3': 33,
                        '4': 44
                    }
                ]
            },
            {
                q: 66,
                r: 55,
                g: [
                    {
                        '3': 30,
                        '4': 40
                    },
                    {
                        '3': 330,
                        '4': 440
                    }
                ]
            }
        ],
        b2: 555
    },
    c: 500,
    d: 600
});

console.log(JSON.stringify(ham, null, 4));
import redis from 'redis';

let REDIS_CLIENT: redis.RedisClient;

export function init(cli: redis.RedisClient) {
    REDIS_CLIENT = cli;
}

function logStoreInCacheError(key: string, reply: string, err: Error | null) {
    console.error(`Error while storing result for key=${key}, result=${reply}`, err);
}

function logGetFromCacheError(err: Error) {
    console.error('Error while retrieving value', err);
}

export function storeInCache(expire: number, key: string, value: any) {
    if(expire && expire > 0) {
        console.log('redis set', key, expire);
        let strValue = JSON.stringify(value);
        REDIS_CLIENT.setex(key, expire, strValue, (err, reply) => {
            if(err || reply != 'OK') {
                logStoreInCacheError(key, reply, err);
            }
        });
    }
}

export function get(key: string) : Promise<{inCache: boolean, result?: any}> {
    return new Promise((resolve) => {
        REDIS_CLIENT.get(key, (err, reply) => {
            if(err) {
                logGetFromCacheError(err);
                resolve({inCache: false});
                return;
            }

            if(!reply) {
                resolve({inCache: false})
                return;
            }

            try{
                let result = JSON.parse(reply);
                resolve({inCache:true, result: result});
                return;
            } catch(ex) {
                logGetFromCacheError(ex);
                resolve({inCache:false});
            }
        });
    });
}

export function createCacheKeyFromObject(obj: {[key:string] : string|number|boolean}, ignoreCase: boolean = false): string {
    let result = Object.keys(obj)
        .sort()
        .map((key) : string => `${encodeURIComponent(key)}=${encodeURIComponent(obj[key])}`);
    if(ignoreCase) {
        result = result.map(s => s.toLowerCase());
    }
    return result.join('&');
}
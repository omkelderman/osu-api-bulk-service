import * as OsuApi from '../OsuApi';
import * as _ from './_shared';

export function getMpBulkFromQuery(query: any) {
    let useObjectMode = _.parseResultModeFromQuery(query);
    let filterStr = _.parseFilterStringFromQuery(query);

    let mps = _.parsePossiblyEncodedNumberArrayFromQuery(query, 'mp');

    return getMpBulk({
        useObjectMode: useObjectMode,
        mps: mps,
        filter: filterStr,
    });
}

export function getMpBulk(req: _.MpBulkRequestParams) {
    return _.getResult(req.useObjectMode, req.mps, req.filter, mp => OsuApi.getMpById(mp));
}
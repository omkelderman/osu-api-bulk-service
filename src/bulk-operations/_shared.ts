import ObjectPropertyFilter from "../ObjectPropertyFilter";
import varint from 'varint';

export interface BaseBulkRequestParams {
    useObjectMode: boolean;
    filter?: string;
}

export interface BeatmapBulkRequestParams extends BaseBulkRequestParams {
    includeConverts: boolean;
    mode?: number;
    mods: number;
    beatmapIds?: number[];
    beatmapHashes?: string[];
    beatmapSetIds?: number[];
}

export interface MpBulkRequestParams extends BaseBulkRequestParams {
    mps: number[];
}

export interface UserBulkRequestParams extends BaseBulkRequestParams {
    mode: number;
    type?: string;
    users: string[] | number[];
}

export function parseMFromQuery<T extends number | undefined>(q: any, defaultVal: T): number | T {
    if (!q.m) return defaultVal;
    var m = parseInt(q.m);
    if (m == NaN || m < 0 || m > 4) throw new Error('invalid m');
    return m;
}

export function parseTypeFromQuery(q: any): string | undefined {
    if (!q.type || !q.type.length) return;
    if (q.type !== 'id' && q.type !== 'string') throw new Error('invalid type');
    return q.type;
}

export function parseNumberFromQuery(q: any, prop: string, defaultVal: number): number {
    if (!q[prop] || !q[prop].length) return defaultVal;
    let n = parseInt(q[prop]);
    if (n == NaN) throw new Error(`parameter ${prop} does not contain a valid number`);
    return n;
}

export function parseRequiredNumberFromQuery(q: any, prop: string): number {
    if (!q[prop] || !q[prop].length) throw new Error(`parameter ${prop} is required`);;
    let n = parseInt(q[prop]);
    if (n == NaN) throw new Error(`parameter ${prop} does not contain a valid number`);
    return n;
}

export function parseStringFromQuery(q: any, prop: string): string {
    if (!q[prop] || !q[prop].length) throw new Error(`parameter ${prop} is required`);
    return q[prop];
}

export function parseFilterStringFromQuery(q: any): string | undefined {
    if (!q.filter || !q.filter.length) return undefined;
    return q.filter;
}

export function parsePossiblyEncodedArrayFromQuery(q: any, prop: string): string[]|number[] {
    if (!q[prop] || (typeof q[prop]) != 'string' || !q[prop].length) return [];
    if(q[prop][0] === '~') {
        return parseEncodedNumberArray(q[prop].substr(1));
    }
    return parseArrayFromQuery(q, prop);
}

export function parsePossiblyEncodedNumberArrayFromQuery(q: any, prop: string): number[] {
    if (!q[prop] || (typeof q[prop]) != 'string' || !q[prop].length) return [];
    if(q[prop][0] === '~') {
        return parseEncodedNumberArray(q[prop].substr(1));
    }
    return parseNumberArrayFromQuery(q, prop);
}

function parseEncodedNumberArray(input: string): number[] {
    let result:number[]=[];
    let buf = Buffer.from(input, 'base64');
    let offset = 0;
    while(offset < buf.length) {
        result.push(varint.decode(buf, offset));
        offset += varint.decode.bytes;
    }
    return result;
}

export function parseArrayOrUndefinedFromQuery(q: any, prop: string): string[] | undefined {
    return parseArrayFromQueryOrDefault(q, prop, undefined);
}
export function parseArrayFromQuery(q: any, prop: string): string[] {
    return parseArrayFromQueryOrDefault(q, prop, [] as string[]);
}

export function parseNumberArrayOrUndefinedFromQuery(q: any, prop: string): number[] | undefined {
    let x = parseArrayOrUndefinedFromQuery(q, prop);
    if (x === undefined) return x;
    return parseStringArrToNumberArr(x, prop);
}
export function parseNumberArrayFromQuery(q: any, prop: string): number[] {
    let x = parseArrayFromQuery(q, prop);
    return parseStringArrToNumberArr(x, prop);
}

function parseStringArrToNumberArr(strArr: string[], prop: string): number[] {
    let arr = strArr.map(str => parseInt(str));
    if (arr.includes(NaN)) throw new Error(`invalid number in parameter ${prop}`);
    return arr;
}

function parseArrayFromQueryOrDefault<T>(q: any, prop: string, defaultVal: T): string[] | T {
    if (!q[prop] || (typeof q[prop]) != 'string' || !q[prop].length) return defaultVal;
    return (q[prop] as string).split(',').map(s => s.trim());
}

export function parseResultModeFromQuery(q: any): boolean {
    if (!q.result_mode || !q.result_mode.length) return false;
    switch (q.result_mode) {
        case 'object': return true;
        case 'array': return false;
        default: throw new Error('invalid result mode');
    }
}

export function parseBooleanFromQuery(q: any, prop: string, defaultVal: boolean = false): boolean {
    if (!q[prop] || !q[prop].length) return defaultVal;
    switch (q[prop]) {
        case '1': return true;
        case '0': return false;
        default: throw new Error(`invalid value for parameter ${prop}`);
    }
}

export async function getObjectModeResult<T extends string | number>(items: T[], filter: ObjectPropertyFilter | undefined, singleItemFunc: (item: T) => Promise<any>): Promise<any> {
    let result: any = {};
    for (let item of items) {
        let itemResult = await getFilteredItemResult(item, filter, singleItemFunc);
        result[item] = itemResult;
    }
    return result;
}

export async function getArrayModeResult<T>(items: T[], filter: ObjectPropertyFilter | undefined, singleItemFunc: (item: T) => Promise<any>): Promise<any[]> {
    let result: any[] = [];
    for (let item of items) {
        let itemResult = await getFilteredItemResult(item, filter, singleItemFunc);
        result.push(itemResult);
    }
    return result;
}

async function getFilteredItemResult<T>(item: T, filter: ObjectPropertyFilter | undefined, singleItemFunc: (item: T) => Promise<any>) {
    let itemResult = await singleItemFunc(item);
    if (filter) {
        if (Array.isArray(itemResult)) {
            itemResult = itemResult.map(x => filter.filterObject(x));
        } else {
            itemResult = filter.filterObject(itemResult);
        }
    }
    return itemResult;
}

export function getResult<T extends string | number>(useObjectMode: boolean, items: T[], filterStr: string | undefined, singleItemFunc: (item: T) => Promise<any>): Promise<any> {
    let filter = filterStr === undefined ? undefined : new ObjectPropertyFilter(filterStr);
    if (useObjectMode) {
        return getObjectModeResult(items, filter, singleItemFunc);
    } else {
        return getArrayModeResult(items, filter, singleItemFunc);
    }
}
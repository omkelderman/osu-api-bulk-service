import * as OsuApi from '../OsuApi';
import * as _ from './_shared';

export function getUsersBulkFromQuery(query: any) {
    let useObjectMode = _.parseResultModeFromQuery(query);
    let filterStr = _.parseFilterStringFromQuery(query);

    let mode = _.parseMFromQuery(query, 0);
    let type = _.parseTypeFromQuery(query);
    let users = _.parsePossiblyEncodedArrayFromQuery(query, 'u');

    return getUsersBulk({
        useObjectMode: useObjectMode,
        users: users,
        filter: filterStr,
        type: type,
        mode: mode
    });
}

export function getUsersBulk(req: _.UserBulkRequestParams) {
    return _.getResult<string | number>(req.useObjectMode, req.users, req.filter, u => OsuApi.getUser(u, req.type, req.mode));
}

import * as OsuApi from '../OsuApi';
import * as _ from './_shared';

export function getBeatmapsBulkFromQuery(query: any) {
    let useObjectMode = _.parseResultModeFromQuery(query);
    let filterStr = _.parseFilterStringFromQuery(query);

    let includeConverts = _.parseBooleanFromQuery(query, 'a');
    let mods = _.parseNumberFromQuery(query, 'mods', 0);
    let mode = _.parseMFromQuery(query, undefined);
    let bArr = _.parsePossiblyEncodedNumberArrayFromQuery(query, 'b');
    let hArr = _.parseArrayFromQuery(query, 'h');
    let sArr = _.parsePossiblyEncodedNumberArrayFromQuery(query, 's');

    return getBeatmapsBulk({
        useObjectMode: useObjectMode,
        filter: filterStr,
        includeConverts: includeConverts,
        mods: mods,
        mode: mode,
        beatmapIds: bArr,
        beatmapHashes: hArr,
        beatmapSetIds: sArr
    });
}

export function getBeatmapsBulk(req: _.BeatmapBulkRequestParams) {
    if (!oneAndOnlyOneArrayHasValues(req.beatmapIds, req.beatmapHashes, req.beatmapSetIds)) {
        throw new Error('specify one and only one of the following parameters: b, h, s');
    }

    if (req.beatmapIds !== undefined && req.beatmapIds.length > 0) {
        // get beatmaps by id
        return _.getResult(req.useObjectMode, req.beatmapIds, req.filter, b => OsuApi.getBeatmapById(b, req.mode, req.includeConverts, req.mods));
    } else if (req.beatmapHashes !== undefined && req.beatmapHashes.length > 0) {
        // get beatmaps by hash
        return _.getResult(req.useObjectMode, req.beatmapHashes, req.filter, h => OsuApi.getBeatmapByHash(h, req.mode, req.includeConverts, req.mods));
    } else if (req.beatmapSetIds !== undefined && req.beatmapSetIds.length > 0) {
        // get beatmaps by set id
        return _.getResult(req.useObjectMode, req.beatmapSetIds, req.filter, s => OsuApi.getBeatmapSetById(s, req.mode, req.includeConverts, req.mods));
    } else {
        return Promise.resolve(undefined);
    }
}

function oneAndOnlyOneArrayHasValues(...arrays: (any[] | undefined)[]): boolean {
    return arrays.filter(x => x !== undefined && x.length > 0).length === 1;
}
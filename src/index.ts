import config from 'config';
import http from 'http';
import killable from 'killable';
import redis from 'redis';
import * as RedisCache from './RedisCache';

// init redis
function initRedis() {
	let redisConfig = config.get<{ db: number, prefix: string, path: string, host: string, port: number }>('redis');
	let redisSettings: redis.ClientOpts = {
		db: redisConfig.db,
		prefix: redisConfig.prefix + ':'
	};
	if (redisSettings.path) {
		redisSettings.path = redisConfig.path;
	} else {
		redisSettings.host = redisConfig.host;
		redisSettings.port = redisConfig.port;
	}
	let redisClient = redis.createClient(redisSettings);
	RedisCache.init(redisClient);
	return redisClient;
}

// init app
import app from './App';
import fs from 'fs';
const listen = config.get<string | number>('http.listen');

function startServer(): Promise<http.Server> {
	return new Promise(resolve => {
		if (typeof listen == 'string') {
			let httpServer = app.listen(listen, () => resolve(httpServer));
		} else {
			let httpServer = app.listen(listen, config.get('http.host'), () => resolve(httpServer));
		}
	});
}

async function initHttp() {
	let httpServer = killable(await startServer());

	// disable timeouts lol, certain bulk requests can take minutes to complete xD
	httpServer.setTimeout(0);

	if (typeof listen == 'string') {
		let socketChmod = config.get<string | number | undefined>('http.socketChmod');
		if (socketChmod) {
			fs.chmodSync(listen, socketChmod);
		}
	}

	console.log('server running', httpServer.address());
	return httpServer;
}

import { EventEmitter } from 'events';

async function run() {
	let redisClient = initRedis();
	let httpServer = await initHttp();

	let x = new EventEmitter();
	process.on('SIGTERM', () => x.emit('requestShutdown'));
	process.on('SIGINT', () => x.emit('requestShutdown'));
	await new Promise(resolve => {
		x.once('requestShutdown', () => {
			x.on('requestShutdown', () => console.log(`already shutting down... (pid: ${process.pid})`));
			resolve();
		});
	})

	console.log('start shutting down things');
	console.log('shutting down http');
	await new Promise((resolve, reject) => httpServer.kill(err => {
		if (err) {
			reject(err);
		} else {
			resolve();
		}
	}));
	console.log('shutting down redis');
	await new Promise(resolve => {
		redisClient.once('end', resolve);
		redisClient.quit();
	});
	console.log(`process with pid ${process.pid} ended gracefully`);
}

run().catch(console.error);
import Koa from 'koa';
import KoaRouter from 'koa-router';
import KoaLogger from 'koa-logger';
import KoaBodyParser from 'koa-bodyparser';
import querystring from 'querystring';

import { getUsersBulkFromQuery } from './bulk-operations/users';
import { getBeatmapsBulkFromQuery } from './bulk-operations/beatmaps';
import { getMpBulkFromQuery } from './bulk-operations/mp';

import { AsyncController } from './controllers/async.controller';
import { RegisterRoutes } from './routes';

function createKoaQueryHandler(getter: (query: any) => Promise<any>) {
    return async (ctx: Koa.ParameterizedContext<any, KoaRouter.IRouterParamContext<any, {}>>) => {
        try {
            ctx.body = await getter(ctx.query);
        } catch (ex) {
            if(ex.osuServerError) {
                // osu server error
                ctx.status = 502;
            } else {
                // other error, lets assume user provided invalid data
                ctx.status = 400;
            }

            if(ex.message) {
                ctx.body = { error: ex.message };
            } else {
                ctx.body = { error: ex };
            }
        }
    };
}

async function getBulkFromQuery(query: any): Promise<any> {
    let result: any = {}
    for (let key of Object.keys(query)) {
        let value = query[key] as string;
        let pipeCharIndex = value.indexOf('|');
        if (pipeCharIndex < 0) {
            throw new Error('malformed querystring');
        }

        let method = value.substr(0, pipeCharIndex);
        let qs = value.substr(pipeCharIndex + 1);
        let q = querystring.parse(decodeURIComponent(qs));
        let localResult: any;

        switch (method) {
            case 'users':
                localResult = await getUsersBulkFromQuery(q);
                break;
            case 'beatmaps':
                localResult = await getBeatmapsBulkFromQuery(q);
                break;
            case 'mp':
                localResult = await getMpBulkFromQuery(q);
                break;
            default: throw new Error('unknown bulk method');
        }

        result[key] = localResult;
    }
    return result;
}

const app = new Koa();
const router = new KoaRouter();
router.get('/', async (ctx) => {
    ctx.body = { test: 'hi' };
});

// old-school routes
router.get('/bulk-users', createKoaQueryHandler(getUsersBulkFromQuery));
router.get('/bulk-beatmaps', createKoaQueryHandler(getBeatmapsBulkFromQuery));
router.get('/bulk-mp', createKoaQueryHandler(getMpBulkFromQuery));
router.get('/bulk', createKoaQueryHandler(getBulkFromQuery));

// fancy new thing
router.use(KoaBodyParser({ enableTypes: ['json'] }));
RegisterRoutes(router);

app.use(KoaLogger());
app.use(router.routes());
app.use(router.allowedMethods());
app.use(async ctx => {
    // ensure json body
    if(ctx.body == null) {
        ctx.body = {status: ctx.status, message: ctx.message};
    }
});

export default app;
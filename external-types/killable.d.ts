declare module 'killable' {
    import http from 'http';

    interface KillableServer extends http.Server {
        kill(cb?: (err?: Error) => void): void;
    }

    function makeKillable(server: http.Server): KillableServer;
    export = makeKillable;
}
